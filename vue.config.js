module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160272/learn_bootstrap/'
      : '/'
}
